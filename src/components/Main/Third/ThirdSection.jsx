import React from 'react';
import style from "./ThirdSection.module.scss"
import BSC from '../../Assets/Image/bsc.webp'
import Aurora from '../../Assets/Image/aurora.webp'
import Zksync from '../../Assets/Image/zksync.webp'
import Polygon from '../../Assets/Image/polygon.webp'
import Gnosis from '../../Assets/Image/gnosis.webp'
import ETH from '../../Assets/Image/eth.webp'


const ThirdSection = () => {
    return (
        <div className={style.container}>
                <h2 className={style.title}>Optimal deals across multiple DEXes on several networks:</h2>
            <div className={style.thirdGroup}>
                <div className={style.lanSection}>
                    <img className={style.imgLan} src={BSC} alt="pic"/>
                    <p className={style.text}>BNB Chain</p>
                </div>
                <div className={style.lanSection}>
                    <img className={style.imgLan} src={Aurora} alt="pic"/>
                    <p className={style.text}>Aurora</p>
                </div>
                <div className={style.lanSection}>
                    <img className={style.imgLan} src={Zksync} alt="pic"/>
                    <p className={style.text}>Zksync</p>
                </div>
                <div className={style.lanSection}>
                    <img className={style.imgLan} src={Polygon} alt="pic"/>
                    <p className={style.text}>Polygon</p>
                </div>
                <div className={style.lanSection}>
                    <img className={style.imgLan} src={Gnosis} alt="pic"/>
                    <p className={style.text}>Gnosis</p>
                </div>
                <div className={style.lanSection}>
                    <img className={style.imgLan} src={ETH} alt="pic"/>
                    <p className={style.text}>ETH</p>
                </div>
            </div>
        </div>
    );
};

export default ThirdSection;
