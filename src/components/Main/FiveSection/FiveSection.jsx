import React from 'react';
import style from './FiveSection.module.scss'
import NFT from '../../Assets/Image/ntfCard.webp'
const FiveSection = () => {
    return (
        <div className={style.container}>
            <div className={style.fiveSection}>
                <div>
                    <img src={NFT} alt="nft"/>
                </div>
                <div className={style.about}>
                    <h2 className={style.title}>NFT CARD COMING SOON:</h2>
                    <p className={style.text}>Embark on an extraordinary journey into the near future with our exclusive NFT card collection. Each card is a digital masterpiece, meticulously crafted to reflect the essence of innovation, imagination, and technological advancement. Experience a diverse range of captivating artworks, each telling its own story and offering a glimpse into a futuristic world filled with possibilities. From visionary landscapes to futuristic characters, our collection invites you to explore the boundless creativity of tomorrow. Whether you're a seasoned collector or a newcomer to the NFT space, our collection offers something for everyone, transcending traditional boundaries and redefining the way we perceive and interact with art. Join us on this groundbreaking adventure as we unveil a new era of digital artistry and redefine the concept of collectibles in the digital age. Welcome to the future – welcome to our NFT card collection</p>
                </div>
            </div>
        </div>
    );
};

export default FiveSection;
