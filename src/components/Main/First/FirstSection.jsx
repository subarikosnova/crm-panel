import React from 'react';
import style from './FirstSection.module.scss'
import Logo from '../../Assets/Image/Logo.png'

const FirstSection = () => {
    return (
        <div className={style.container}>
            <div className={style.section}>
                <div className={style.logoSection}>
                    <img className={style.logo} src={Logo} alt="Logo"/>
                    <h2 className={style.title}>
                        Blueberry Coin</h2>
                </div>

                <div className={style.infoSection}>
                    <h3 className={style.subTitle}>Vitamins that boost your wallet</h3>
                    <p className={style.text}>A fresh addition to the Fruit crypto ecosystem. Join now to get ahead!</p>
                </div>

            </div>
        </div>
    );
};

export default FirstSection;
