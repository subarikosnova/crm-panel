import React from 'react';
import style from './SecondSection.module.scss'
import Buy from '../../Assets/Image/buy.png'
import Transaction from '../../Assets/Image/trans.png'
import Social from '../../Assets/Image/social.png'



const SecondSection = () => {
    return (
        <div className={style.container}>
                 <h2 className={style.title}>Join a community of millions:</h2>
            <div className={style.section}>
                <div className={style.info}>
                    <div className={style.infoItem}>
                        <img className={style.infoItemImg} src={Buy} alt="icon"/>
                        <p>1.6 Millions</p>
                        <p>Buy coin's per day</p>
                    </div>
                    <div className={style.infoItem}>
                        <img className={style.infoItemImg} src={Transaction} alt="icon"/>
                        <p>23.805</p>
                        <p>Transactions per hour</p>
                    </div>
                    <div className={style.infoItem}>
                        <img className={style.infoItemImg} src={Social} alt="icon"/>
                        <p>2.4 Millions</p>
                        <p>Community in social</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SecondSection;
