import React from 'react';
import style from './FourthSection.module.scss'
import EcoSys from '../../Assets/Image/ecoSys.webp'
const FourthSection = () => {
    return (
        <div className={style.container}>
            <h2 className={style.title}>Last statistic of month:</h2>
            <div className={style.fourthSection}>
                <div className={style.firstItem}>
                    <div className={style.infoBlock}>
                        <p className={style.subTitle}>Coins left in circulation:</p>

                        <div className={style.circleDiagram}>87.5%</div>
                    </div>
                </div>

                <div className={style.secondItem}>
                    <div className={style.infoBlockMini}>
                        <p className={style.subTitle}>Epoch: 31 [end 24 june]</p>
                        <span className={style.line}></span>
                    </div>
                    <div className={style.infoBlockMacro}>
                        <div className={style.subTitleWrapper}>
                            <p className={style.subTitle}>True TPS: last 30 days</p>
                        </div>
                        <div className={style.block}>
                            <span className={style.greenBlock}></span>
                            <span className={style.greenBlock}></span>
                            <span className={style.greenBlock1}></span>
                            <span className={style.greenBlock2}></span>
                            <span className={style.greenBlock3}></span>
                            <span className={style.greenBlock2}></span>
                            <span className={style.greenBlock3}></span>
                            <span className={style.greenBlock1}></span>
                            <span className={style.greenBlock}></span>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    );
};

export default FourthSection;
