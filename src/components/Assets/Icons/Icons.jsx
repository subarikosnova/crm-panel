import React from 'react';
import OpenImg from '../Image/menu.png'
import CloseImg from '../Image/close.png'


export const Open = () => {
    return (
        <img style={{width: "24px", height: "24px"}}
             src={OpenImg} alt="menu"/>
    );
};

export const Close = () => {
    return (
        <div>
            <img style={{width: "24px", height: "24px"}}
                 src={CloseImg} alt="menu"/>
        </div>
    );
};

