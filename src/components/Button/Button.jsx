import React from 'react';
import './Button.scss';

const Button = ({ title }) => {
    return (
        <button data-title={title}>
            {title}
        </button>
    );
};

export default Button;
