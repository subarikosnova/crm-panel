import React from 'react';
import style from './Footer.module.scss'
const Footer = () => {
    return (
        <div className={style.container}>
            <div className={style.footer}>
                    <ul className={style.list}>
                        <li><a className={style.text} href="#">Home</a></li>
                        <li><a className={style.text} href="#">About Us</a></li>
                        <li><a className={style.text} href="#">Services</a></li>
                        <li><a className={style.text} href="#">Contact</a></li>
                    </ul>
                <p className={style.subTitle}>All right reserved 2024 ®</p>
            </div>
        </div>
    );
};

export default Footer;
