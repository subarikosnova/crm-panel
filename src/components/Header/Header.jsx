import React, { useState, useEffect } from 'react';
import style from './Header.module.scss';
import { Open, Close } from '../Assets/Icons/Icons';
import Button from "../Button/Button";

const Header = () => {
    const [menuOpen, setMenuOpen] = useState(window.innerWidth >= 1200);



    useEffect(() => {
        const handleResize = () => {

            setMenuOpen(window.innerWidth >= 1200);
        };

        window.addEventListener('resize', handleResize);

        return () => {
            window.removeEventListener('resize', handleResize);
        };
    }, []);

    const handleOpen = () => {
        setMenuOpen(true);


    };

    const handleClose = () => {
        setMenuOpen(false);
    };

    return (
        <div className={style.container}>
            <div className={style.header}>

                <div className={style.headerButton}>
                    {menuOpen ? (
                        <div className={style.btnMenu} onClick={handleClose}><Close/></div>
                    ) : (
                        <div className={style.btnMenu} onClick={handleOpen}><Open/></div>
                    )}
                </div>

            </div>
            {menuOpen && (
                <div className={style.menu}>
                    <Button title="Home"/>
                    <Button title="Contact"/>
                    <Button title="NFT"/>
                    <Button title="About"/>
                    {/*<Button title="Price"/>*/}
                    {/*<Button title="Roadmap"/>*/}
                </div>
            )}
        </div>
    );
};

export default Header;
