import React from "react";
import style from "./style/_base.scss"
import Header from "./components/Header/Header";
import FirstSection from "./components/Main/First/FirstSection";
import SecondSection from "./components/Main/Second/SecondSection";
import ThirdSection from "./components/Main/Third/ThirdSection";
import FourthSection from "./components/Main/Fourth/FourthSection";
import FiveSection from "./components/Main/FiveSection/FiveSection";
import Footer from "./components/Footer/Footer";




function App() {
  return (
      <div className={style.appWrapper}>
          <Header/>
          <FirstSection/>
          <SecondSection/>
          <ThirdSection/>
          <FourthSection/>
          <FiveSection/>
          <Footer/>
      </div>
  );
}

export default App;
